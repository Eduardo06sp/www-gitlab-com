---
layout: markdown_page
title: "Ray Leihe's README (VP Marketing Strategy & Platforms)"
description: "Learn more about working with Ray Leihe"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## About me

Hi, I am Ray, and I lead the Marketing Strategy & Platforms team.

I joined GitLab from [Cloudflare](https://www.cloudflare.com/), where I built and led the marketing technology & platform group, including analytics & measurement, data platform, globalization, marketing operations and web experience, through IPO and to massive scale. Prior to Cloudflare, I led business operations, analytics & data science for [LinkedIn](http://www.linkedin.com/)'s [Marketing Solutions](https://business.linkedin.com/marketing-solutions) & [Sales Solutions](https://business.linkedin.com/sales-solutions) business lines, which grew from $250 million to $1 billion during my tenure. I am especially inspired by GitLab's mission of "everyone can contribute," which resonates so well with my passion for creating and investing in products & services that empower individuals.

[LinkedIn](https://www.linkedin.com/in/rleihe/?lipi=urn%3Ali%3Apage%3Ad_flagship3_feed%3BDWl5%2BpzQTJOxOyNRm4BQ4A%3D%3D)

### Interests

Outside of work, I enjoy skiing in Tahoe, hiking in the Santa Cruz redwoods, and playing Texas Holdem Poker. I live in Mountain View, CA with my wife Jane, teenage daughters Emma & Nicole, and our beloved Beagle Italian Greyhound mix Meron.

### My 2 favorite quotes are:

1. “If you want to go fast, go alone. If you want to go far, go together”.
2. “Don't ask yourself what the world needs. Ask yourself what makes you come alive, and go do that, because what the world needs is people who have come alive”.

## How you can help me

- I believe time is the most precious and finite resource for each of us. So efficient use of time and high quality time spent is my ultimate #1 priority.
- Provide me with specific help needed, context and background.
- I value unique insights, especially if you show how dots are connected. I appreciate contrarian views.

## My working style

I define success along these 4 dimensions:

1. **Achievement** - have worked hard to accomplish stretch goals.
2. **Significance** - context of achievement & impact that can change the trajectory of an organization or a person.
3. **Happiness** - feeling of fulfillment; look forward to coming to work every morning and look forward to going home every evening.
4. **Legacy** - what we leave behind us - develop bench strength and other leaders; positive addition to organization culture and ecosystem.

### What excites me:

- Team chemistry and trust.
- Experiment with new things & learn from the process.
- Power of belief, perseverance, and compounding.
- When strategic and venture bets pay off.

### My CliftonStrengths signature themes:

1. **Learner**: I am energized by the steady and deliberate journey from ignorance to competence.
2. **Individualization**: I am a keen observer of other people’s strengths. I can draw out the best in each person.
3. **Ideation**: I am delighted when I discover beneath the complex surface an elegantly simple concept to explain why things are the way they are.
4. **Self-Assurance**: I have confidence not only in my abilities but in my judgment. I know that my perspective is unique and distinct. I am not easily swayed by someone else’s arguments.
5. **Maximizer**: Excellence, not average, is my measure.

### Other important tips:

- I prefer to play a long game and focus on long-term success beyond one quarter and one year.
- I do not work well under micro-management.
- I can be impatient with “high maintenance” folks.
- I value “reliability” the most in the [Trust equation](https://modelthinkers.com/mental-model/trust-equation).
- Please follow-through. No surprises.

## What I assume about others

- You bring the best version of yourself to work every day. Still, there are also other important things and relationships in your life.
- You’re transparent, with no hidden agenda when we interact.
- You’re motivated by a common vision and the larger organization's success.
- You’re competent in your domains and are better at your job than I am.
- Each person brings a unique background, talent, strength, and story to the table.

## Communicating with me

- I am flexible in terms of communication channels. However, I am not very good at being an active listener, providing useful comments, and taking notes simultaneously.
- I learn and process best with visuals, key bullet points and videos. I may get lost in rows & columns filled with numbers and pages of text.
- I may be quiet when I am in absorbing mode, thinking, and “zooming out.” I ask detailed questions when I am “zooming in.”
- For more formal packaged communication, I prefer a synthesized summary backed by solid details and clear key assumptions.
- I am allergic to jargon and buzzwords.

Thanks for reading! I hope this Read Me has helped you get to know me. If there’s anything else you’d like to know, please schedule a coffee chat!